package controllers

import (
	"database/sql"

	"github.com/hako/branca"
)

// Controller contains the core business logic separated from the transport layer.
type Controller struct {
	db    *sql.DB
	codec *branca.Branca
}

// New Controller implementation.
func New(db *sql.DB, codec *branca.Branca) *Controller {
	return &Controller{
		db:    db,
		codec: codec,
	}
}

/*********************** INFO ***********************
	Create work with db similar handlers getdata
*****************************************************/
