package main

import (
	"database/sql"
	"flag"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"projects/golang-webpack-template/controllers"
	"projects/golang-webpack-template/handlers"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/go-webpack/webpack"
	"github.com/gobuffalo/packr"
	"github.com/hako/branca"
	"github.com/joho/godotenv"
	"github.com/qor/render"
	migrate "github.com/rubenv/sql-migrate"

	// This is postgres drivers
	_ "github.com/jackc/pgx/stdlib"
)

var (
	isMigrations *bool
	isDev        *bool
	localHost    string
	port         string
	dsn          string
	db           *sql.DB
	codec        *branca.Branca
	renderer     *render.Render
	router       *gin.Engine
)

func init() {
	// Logging to a file.
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f)

	isMigrations = flag.Bool("migrate", false, "migrations mode")
	isDev = flag.Bool("dev", false, "development mode")
	flag.Parse()

	// Init ENV variables
	if err := godotenv.Load("config.env"); err != nil {
		panic(err)
	}

	localHost = os.Getenv("LOCALHOST")
	port = os.Getenv("PORT")
	dsn = os.Getenv("DSN")

	// For start on heroku
	herokuDSN := os.Getenv("DATABASE_URL")
	if len(herokuDSN) > 0 {
		dsn = os.Getenv("DATABASE_URL")
	}

	// For render templates
	renderer = render.New(&render.Config{
		ViewPaths: []string{"templates"},
		FuncMapMaker: func(*render.Render, *http.Request, http.ResponseWriter) template.FuncMap {
			return map[string]interface{}{"asset": webpack.AssetHelper}
		},
	})
}

func main() {
	connectDataBase()
	startMigrationUp()
	webpackInit()
	brancaCodecInit()

	// Create controllers&handlers
	controller := controllers.New(db, codec)
	handler := handlers.New(controller)

	// Create router
	router = gin.Default()

	routerServices()
	getPathURL()
	routerHandlers(handler)

	router.Run(":5000")
}

// Connect database
func connectDataBase() {
	db, err := sql.Open("pgx", dsn)
	defer db.Close()
	if err != nil {
		panic("ERROR CONNECT TO DATABASE! CHECK PARAMS FOR CONNECT (url, password and more...)")
	}
	err = db.Ping()
	if err != nil {
		log.Printf("ERROR DB PING:%s\n", err)
		panic(err)
	}
	log.Println("CONNECT DATABASE IS SUCCESS")
}

// For webpack hot reload and add path to assets in template
func webpackInit() {
	webpack.FsPath = "./public/assets"
	webpack.WebPath = "manifest"
	webpack.Plugin = "manifest"
	webpack.Init(*isDev)
}

// For migration
func startMigrationUp() {
	if *isMigrations {
		migrations := &migrate.PackrMigrationSource{
			Box: packr.NewBox("./migrations"),
		}
		n, err := migrate.Exec(db, "postgres", migrations, migrate.Up)
		if err != nil {
			log.Printf("Mirgation is error: %s", err)
		}
		log.Printf("Applied %d migrations!\n", n)
		return
	}
}

// Branca is a secure alternative to JWT
func brancaCodecInit() {
	codec = branca.NewBranca(os.Getenv("BRANCA"))
	codec.SetTTL(uint32(controllers.TokenLifespan.Seconds()))
}

// Middlewares gin
func routerServices() {
	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	router.Use(gin.Recovery())
	// Serving static files
	router.Use(static.Serve("/", static.LocalFile("public", false)))
	// Gin debug mode
	gin.SetMode(gin.DebugMode)
}

// For paste template home in application
func tmpl(ctx *gin.Context) {
	renderer.Execute(
		"home",
		gin.H{},
		ctx.Request,
		ctx.Writer,
	)
}

// List routers for get templte
func getPathURL() {
	router.GET("/", tmpl)
	router.GET("/page", tmpl)
}

// List routers for logic handlers
func routerHandlers(handler *handlers.Handler) {
	router.POST("/getdata", handler.GetData)
}
