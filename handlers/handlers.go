package handlers

import "projects/golang-webpack-template/controllers"

// Handler struct for work with routers
type Handler struct {
	*controllers.Controller
}

// New makes use of the service to provide an http.Handler with predefined routing.
func New(c *controllers.Controller) *Handler {
	h := &Handler{c}
	return h
}
