import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App.vue'
import Main from './view/ui-main.vue'
import Page from './view/ui-page.vue'

const routes = [
  {
    path: '/', component: Main
  },
  {
    path: '/page', component: Page
  }
]

const router = new VueRouter({
  mode: "history",
  routes
})

Vue.use(VueRouter)

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
